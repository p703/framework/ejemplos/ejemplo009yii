<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "conduce".
 *
 * @property string $camionero
 * @property string $camiones
 *
 * @property Camioneros $camionero0
 * @property Camiones $camiones0
 */
class Conduce extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conduce';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['camionero', 'camiones'], 'required'],
            [['camionero', 'camiones'], 'string', 'max' => 30],
            [['camionero', 'camiones'], 'unique', 'targetAttribute' => ['camionero', 'camiones']],
            [['camiones'], 'exist', 'skipOnError' => true, 'targetClass' => Camiones::className(), 'targetAttribute' => ['camiones' => 'matricula']],
            [['camionero'], 'exist', 'skipOnError' => true, 'targetClass' => Camioneros::className(), 'targetAttribute' => ['camionero' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'camionero' => 'Camionero',
            'camiones' => 'Camiones',
        ];
    }

    /**
     * Gets query for [[Camionero0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamionero0()
    {
        return $this->hasOne(Camioneros::className(), ['dni' => 'camionero']);
    }

    /**
     * Gets query for [[Camiones0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamiones0()
    {
        return $this->hasOne(Camiones::className(), ['matricula' => 'camiones']);
    }
}
