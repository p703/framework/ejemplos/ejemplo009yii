<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "camiones".
 *
 * @property int|null $potencia
 * @property string $matricula
 * @property string|null $modelo
 * @property string|null $tipo
 *
 * @property Camioneros[] $camioneros
 * @property Conduce[] $conduces
 */
class Camiones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'camiones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['potencia'], 'integer'],
            [['matricula'], 'required'],
            [['matricula', 'tipo'], 'string', 'max' => 30],
            [['modelo'], 'string', 'max' => 100],
            [['matricula'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'potencia' => 'Potencia',
            'matricula' => 'Matricula',
            'modelo' => 'Modelo',
            'tipo' => 'Tipo',
        ];
    }

    /**
     * Gets query for [[Camioneros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamioneros()
    {
        return $this->hasMany(Camioneros::className(), ['dni' => 'camionero'])->viaTable('conduce', ['camiones' => 'matricula']);
    }

    /**
     * Gets query for [[Conduces]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConduces()
    {
        return $this->hasMany(Conduce::className(), ['camiones' => 'matricula']);
    }
}
