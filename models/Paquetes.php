<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paquetes".
 *
 * @property int $codigo
 * @property string|null $descripcion
 * @property string|null $destinatario
 * @property string|null $direccion
 *
 * @property Camioneros[] $camioneros
 * @property Destinado $destinado
 * @property Distribuye $distribuye
 * @property Provincias[] $provincias
 */
class Paquetes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paquetes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo'], 'integer'],
            [['descripcion'], 'string', 'max' => 500],
            [['destinatario'], 'string', 'max' => 100],
            [['direccion'], 'string', 'max' => 200],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'descripcion' => 'Descripcion',
            'destinatario' => 'Destinatario',
            'direccion' => 'Direccion',
        ];
    }

    /**
     * Gets query for [[Camioneros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamioneros()
    {
        return $this->hasMany(Camioneros::className(), ['dni' => 'camionero'])->viaTable('distribuye', ['paquetes' => 'codigo']);
    }

    /**
     * Gets query for [[Destinado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDestinado()
    {
        return $this->hasOne(Destinado::className(), ['paquetes' => 'codigo']);
    }

    /**
     * Gets query for [[Distribuye]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDistribuye()
    {
        return $this->hasOne(Distribuye::className(), ['paquetes' => 'codigo']);
    }

    /**
     * Gets query for [[Provincias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvincias()
    {
        return $this->hasMany(Provincias::className(), ['codigo' => 'provincia'])->viaTable('destinado', ['paquetes' => 'codigo']);
    }
}
