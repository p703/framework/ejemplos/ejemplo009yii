<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "destinado".
 *
 * @property int $provincia
 * @property int $paquetes
 *
 * @property Paquetes $paquetes0
 * @property Provincias $provincia0
 */
class Destinado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'destinado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['provincia', 'paquetes'], 'required'],
            [['provincia', 'paquetes'], 'integer'],
            [['paquetes'], 'unique'],
            [['provincia', 'paquetes'], 'unique', 'targetAttribute' => ['provincia', 'paquetes']],
            [['paquetes'], 'exist', 'skipOnError' => true, 'targetClass' => Paquetes::className(), 'targetAttribute' => ['paquetes' => 'codigo']],
            [['provincia'], 'exist', 'skipOnError' => true, 'targetClass' => Provincias::className(), 'targetAttribute' => ['provincia' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'provincia' => 'Provincia',
            'paquetes' => 'Paquetes',
        ];
    }

    /**
     * Gets query for [[Paquetes0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetes0()
    {
        return $this->hasOne(Paquetes::className(), ['codigo' => 'paquetes']);
    }

    /**
     * Gets query for [[Provincia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvincia0()
    {
        return $this->hasOne(Provincias::className(), ['codigo' => 'provincia']);
    }
    
    public function getPaquetes(){
        //realizo consulta que devuelve array con todos los paquetes
        $salida= Paquetes::find()->all();
        
        //devolver un array cuyo indice son los codigos de los paquetes y los valores son las descripciones
        return \yii\helpers\ArrayHelper::map($salida,"codigo","descripcion");
    }
    
    public function getProvincias(){
        //realizo consulta que devuelve array con todos las provincias
        $salida= Provincias::find()->all();
        
        //devolver un array cuyo indice son los codigos de las provincias y los valores el nombre de la provincia
        return \yii\helpers\ArrayHelper::map($salida,"codigo",function($modelo){
            return $modelo->codigo . " -- " . $model->nombre;
        });
    }

}
