<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distribuye".
 *
 * @property string $camionero
 * @property int $paquetes
 *
 * @property Camioneros $camionero0
 * @property Paquetes $paquetes0
 */
class Distribuye extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'distribuye';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['camionero', 'paquetes'], 'required'],
            [['paquetes'], 'integer'],
            [['camionero'], 'string', 'max' => 30],
            [['paquetes'], 'unique'],
            [['camionero', 'paquetes'], 'unique', 'targetAttribute' => ['camionero', 'paquetes']],
            [['camionero'], 'exist', 'skipOnError' => true, 'targetClass' => Camioneros::className(), 'targetAttribute' => ['camionero' => 'dni']],
            [['paquetes'], 'exist', 'skipOnError' => true, 'targetClass' => Paquetes::className(), 'targetAttribute' => ['paquetes' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'camionero' => 'Camionero',
            'paquetes' => 'Paquetes',
        ];
    }

    /**
     * Gets query for [[Camionero0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamionero0()
    {
        return $this->hasOne(Camioneros::className(), ['dni' => 'camionero']);
    }

    /**
     * Gets query for [[Paquetes0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetes0()
    {
        return $this->hasOne(Paquetes::className(), ['codigo' => 'paquetes']);
    }
}
