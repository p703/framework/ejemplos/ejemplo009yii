<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "camioneros".
 *
 * @property string|null $poblacion
 * @property int|null $telefono
 * @property string|null $direccion
 * @property string|null $nombre
 * @property string $dni
 *
 * @property Camiones[] $camiones
 * @property Conduce[] $conduces
 * @property Distribuye[] $distribuyes
 * @property Paquetes[] $paquetes
 */
class Camioneros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'camioneros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telefono'], 'integer'],
            [['dni'], 'required'],
            [['poblacion', 'direccion', 'nombre'], 'string', 'max' => 100],
            [['dni'], 'string', 'max' => 30],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'poblacion' => 'Poblacion',
            'telefono' => 'Telefono',
            'direccion' => 'Direccion',
            'nombre' => 'Nombre',
            'dni' => 'Dni',
        ];
    }

    /**
     * Gets query for [[Camiones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamiones()
    {
        return $this->hasMany(Camiones::className(), ['matricula' => 'camiones'])->viaTable('conduce', ['camionero' => 'dni']);
    }

    /**
     * Gets query for [[Conduces]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConduces()
    {
        return $this->hasMany(Conduce::className(), ['camionero' => 'dni']);
    }

    /**
     * Gets query for [[Distribuyes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDistribuyes()
    {
        return $this->hasMany(Distribuye::className(), ['camionero' => 'dni']);
    }

    /**
     * Gets query for [[Paquetes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetes()
    {
        return $this->hasMany(Paquetes::className(), ['codigo' => 'paquetes'])->viaTable('distribuye', ['camionero' => 'dni']);
    }
}
