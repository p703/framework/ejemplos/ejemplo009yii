<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuye */

$this->title = $model->camionero;
$this->params['breadcrumbs'][] = ['label' => 'Distribuyes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="distribuye-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'camionero' => $model->camionero, 'paquetes' => $model->paquetes], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'camionero' => $model->camionero, 'paquetes' => $model->paquetes], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'camionero',
            'paquetes',
        ],
    ]) ?>

</div>
