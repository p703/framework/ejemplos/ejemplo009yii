<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuye */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="distribuye-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'camionero')->dropDownList($listadoCamionero) ?>

    <?= $form->field($model, 'paquetes')->dropDownList($listadoPaquetes) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
