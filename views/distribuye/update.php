<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuye */

$this->title = 'Update Distribuye: ' . $model->camionero;
$this->params['breadcrumbs'][] = ['label' => 'Distribuyes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->camionero, 'url' => ['view', 'camionero' => $model->camionero, 'paquetes' => $model->paquetes]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="distribuye-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listadoCamionero'=>$listadoCamionero,
        'listadoPaquetes'=>$listadoPaquetes
    ]) ?>

</div>
