<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Conduce */

$this->title = 'Update Conduce: ' . $model->camionero;
$this->params['breadcrumbs'][] = ['label' => 'Conduces', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->camionero, 'url' => ['view', 'camionero' => $model->camionero, 'camiones' => $model->camiones]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="conduce-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listadoCamionero'=>$listadoCamionero,
        'listadoCamiones'=>$listadoCamiones
    ]) ?>

</div>
