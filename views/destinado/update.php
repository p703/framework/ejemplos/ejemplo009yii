<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Destinado */

$this->title = 'Update Destinado: ' . $model->provincia;
$this->params['breadcrumbs'][] = ['label' => 'Destinados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->provincia, 'url' => ['view', 'provincia' => $model->provincia, 'paquetes' => $model->paquetes]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="destinado-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listadoProvincias'=>$listadoProvincias,
        'listadoPaquetes'=>$listadoPaquetes
    ]) ?>

</div>
