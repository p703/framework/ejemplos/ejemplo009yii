<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Destinado */

$this->title = $model->provincia;
$this->params['breadcrumbs'][] = ['label' => 'Destinados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="destinado-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'provincia' => $model->provincia, 'paquetes' => $model->paquetes], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'provincia' => $model->provincia, 'paquetes' => $model->paquetes], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'provincia',
            'paquetes',
        ],
    ]) ?>

</div>
