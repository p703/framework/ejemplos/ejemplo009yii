<?php

namespace app\controllers;

use app\models\Destinado;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Provincias;
use app\models\Paquetes;
use yii\helpers\ArrayHelper;

/**
 * DestinadoController implements the CRUD actions for Destinado model.
 */
class DestinadoController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Destinado models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Destinado::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'provincia' => SORT_DESC,
                    'paquetes' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Destinado model.
     * @param int $provincia Provincia
     * @param int $paquetes Paquetes
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($provincia, $paquetes)
    {
        return $this->render('view', [
            'model' => $this->findModel($provincia, $paquetes),
        ]);
    }

    /**
     * Creates a new Destinado model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Destinado();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'provincia' => $model->provincia, 'paquetes' => $model->paquetes]);
            }
        } else {
            $model->loadDefaultValues();
        }
        $provincias= Provincias::find()->all();
        $paquetes= Paquetes::find()->all();
       

        $listadoProvincias= ArrayHelper::map($provincias, 'codigo', 'nombre');
        $listadoPaquetes= ArrayHelper::map($paquetes, 'codigo', 'destinatario');
        
        return $this->render('create', [
            'model' => $model,
            'listadoProvincias'=>$listadoProvincias,
            'listadoPaquetes'=>$listadoPaquetes
        ]);
    }

    /**
     * Updates an existing Destinado model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $provincia Provincia
     * @param int $paquetes Paquetes
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($provincia, $paquetes)
    {
        $model = $this->findModel($provincia, $paquetes);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'provincia' => $model->provincia, 'paquetes' => $model->paquetes]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Destinado model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $provincia Provincia
     * @param int $paquetes Paquetes
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($provincia, $paquetes)
    {
        $this->findModel($provincia, $paquetes)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Destinado model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $provincia Provincia
     * @param int $paquetes Paquetes
     * @return Destinado the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($provincia, $paquetes)
    {
        if (($model = Destinado::findOne(['provincia' => $provincia, 'paquetes' => $paquetes])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
