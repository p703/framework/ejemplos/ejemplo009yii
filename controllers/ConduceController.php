<?php

namespace app\controllers;

use app\models\Conduce;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Camioneros;
use app\models\Camiones;
use yii\helpers\ArrayHelper;

/**
 * ConduceController implements the CRUD actions for Conduce model.
 */
class ConduceController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Conduce models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Conduce::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'camionero' => SORT_DESC,
                    'camiones' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Conduce model.
     * @param string $camionero Camionero
     * @param string $camiones Camiones
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($camionero, $camiones)
    {
        return $this->render('view', [
            'model' => $this->findModel($camionero, $camiones),
        ]);
    }

    /**
     * Creates a new Conduce model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Conduce();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'camionero' => $model->camionero, 'camiones' => $model->camiones]);
            }
        } else {
            $model->loadDefaultValues();
        }
        //variable tipo array con todos los camioneros y camiones 
        $camionero= Camioneros::find()->all();
        $camiones= Camiones::find()->all();
       
        //crear un array donde el indice sea el dni del camionero y el valor el nombre
        $listadoCamionero= ArrayHelper::map($camionero, 'dni', 'nombre');
        $listadoCamiones= ArrayHelper::map($camiones, 'matricula', 'modelo');
        
        
        return $this->render('create', [
            'model' => $model,
            'listadoCamionero'=>$listadoCamionero,
            'listadoCamiones'=>$listadoCamiones
            
        ]);
    }

    /**
     * Updates an existing Conduce model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $camionero Camionero
     * @param string $camiones Camiones
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($camionero, $camiones)
    {
        $model = $this->findModel($camionero, $camiones);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'camionero' => $model->camionero, 'camiones' => $model->camiones]);
        }
        
        $camionero= Camioneros::find()->all();
        $camiones= Camiones::find()->all();
       
        //crear un array donde el indice sea el dni del camionero y el valor el nombre
        $listadoCamionero= ArrayHelper::map($camionero, 'dni', 'nombre');
        $listadoCamiones= ArrayHelper::map($camiones, 'matricula', 'modelo');
        
        return $this->render('update', [
            'model' => $model,
            'listadoCamionero'=>$listadoCamionero,
            'listadoCamiones'=>$listadoCamiones
        ]);
    }

    /**
     * Deletes an existing Conduce model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $camionero Camionero
     * @param string $camiones Camiones
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($camionero, $camiones)
    {
        $this->findModel($camionero, $camiones)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Conduce model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $camionero Camionero
     * @param string $camiones Camiones
     * @return Conduce the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($camionero, $camiones)
    {
        if (($model = Conduce::findOne(['camionero' => $camionero, 'camiones' => $camiones])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
